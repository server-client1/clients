package com.dotin.entity;

import java.io.Serializable;

public class Transaction implements Serializable {
    private static final long serialVersionUID = 6529685098267757690L;
    private String transactionId;
    private TransactionType transactionType;
    private String amount;
    private String depositId;

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }


    public TransactionType getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(TransactionType transactionType) {
        this.transactionType = transactionType;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getDepositId() {
        return depositId;
    }

    public void setDepositId(String depositId) {
        this.depositId = depositId;
    }


}
