package com.dotin.entity;

public enum TerminalType {
    ATM;

    public static TerminalType convert(String terminalTypeValue) {
        for (TerminalType terminalType : TerminalType.values()) {
            if (terminalType.name().equalsIgnoreCase(terminalTypeValue))
                return terminalType;

        }
        return null;

    }
}
