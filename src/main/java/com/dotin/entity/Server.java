package com.dotin.entity;

public class Server {
    private String serverIp;
    private String serverPortNumber;


    public String getServerIp() {
        return serverIp;
    }

    public void setServerIp(String serverIp) {
        this.serverIp = serverIp;
    }

    public String getServerPortNumber() {
        return serverPortNumber;
    }

    public void setServerPortNumber(String serverPortNumber) {
        this.serverPortNumber = serverPortNumber;
    }
}
