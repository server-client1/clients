package com.dotin.common;

import com.dotin.entity.*;
import org.w3c.dom.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.*;
import java.io.*;

import java.net.InetAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class Clients implements Runnable {
    private final static Logger logger = Logger.getLogger(Clients.class.getName());
    private static Socket socket;
    private static boolean waitForServer = false;
    private String clientInformationFileAddress;
    private String transactionResponseFileAddress;

    public String getClientInformationFileAddress() {
        return clientInformationFileAddress;
    }

    public Clients(String clientInformationFileAddress, String transactionResponseFileAddress) {
        this.clientInformationFileAddress = clientInformationFileAddress;
        this.transactionResponseFileAddress = transactionResponseFileAddress;
    }

    public void setClientInformationFileAddress(String clientInformationFileAddress) {
        this.clientInformationFileAddress = clientInformationFileAddress;
    }

    public String getTransactionResponseFileAddress() {
        return transactionResponseFileAddress;
    }

    public void setTransactionResponseFileAddress(String transactionResponseFileAddress) {
        this.transactionResponseFileAddress = transactionResponseFileAddress;
    }


    private void writeXml(List<Response> responseList) {
        logger.entering("Clients", "writeXml");

        DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = null;
        try {
            documentBuilder = documentFactory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
            logger.throwing("Clients Class", "writeXml", e);
        }
        Document document = documentBuilder.newDocument();
        // root element
        Element rootElement = document.createElement("Transactions");
        document.appendChild(rootElement);
        for (int i = 0; i < responseList.size(); i++) {
            Response response = responseList.get(i);
            Element transaction = document.createElement("transaction");
            rootElement.appendChild(transaction);
            // set an attribute to staff element
            Attr firstAttr = document.createAttribute("Type");
            System.out.println(response.getTransactionType());
            firstAttr.setValue(response.getTransactionType());
            transaction.setAttributeNode(firstAttr);
            Attr secondAttr = document.createAttribute("Result");
            secondAttr.setValue(response.getTransactionResult());
            transaction.setAttributeNode(secondAttr);
            Attr thirdAttr = document.createAttribute("finalBalance");
            thirdAttr.setValue(response.getFinalBalance());
            transaction.setAttributeNode(thirdAttr);
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = null;
            try {
                transformer = transformerFactory.newTransformer();
            } catch (TransformerConfigurationException e) {
                e.printStackTrace();
                logger.throwing("Clients Class", "writeXml", e);
            }
            DOMSource domSource = new DOMSource(document);
            StreamResult streamResult = new StreamResult(new File(this.transactionResponseFileAddress));
            try {
                transformer.transform(domSource, streamResult);
            } catch (TransformerException e) {
                e.printStackTrace();
                logger.throwing("Clients Class", "writeXml", e);
            }

            logger.exiting("Clients", "writeXml", "create Response File");

        }

    }


    private Client readXml() throws Exception {
        List<Transaction> transactionList = new ArrayList<>();
        Client client = new Client();
        Terminal terminal = new Terminal();
        Server server = new Server();
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(new File(this.clientInformationFileAddress));
        document.getDocumentElement().normalize();
        XPath xpath = XPathFactory.newInstance().newXPath();
        XPathExpression xPathExpression = xpath.compile("//terminal/transactions/transaction");
        NodeList nodeList = (NodeList) xPathExpression.evaluate(document, XPathConstants.NODESET);
        terminal.setTerminalId(document.getElementsByTagName("terminal").item(0).getAttributes().getNamedItem("id").getTextContent());
        String terminalType = document.getElementsByTagName("terminal").item(0).getAttributes().getNamedItem("type").getTextContent();
        terminal.setTerminalType(TerminalType.convert(terminalType));
        server.setServerIp(document.getElementsByTagName("server").item(0).getAttributes().getNamedItem("ip").getTextContent());
        server.setServerPortNumber(document.getElementsByTagName("server").item(0).getAttributes().getNamedItem("port").getTextContent());
        for (int i = 0; i < nodeList.getLength(); i++) {
            Transaction transaction = new Transaction();
            Node transactionNode = nodeList.item(i);
            transaction.setTransactionId(transactionNode.getAttributes().getNamedItem("id").getTextContent());
            transaction.setDepositId(transactionNode.getAttributes().getNamedItem("deposit").getTextContent());
            transaction.setAmount(transactionNode.getAttributes().getNamedItem("amount").getTextContent());
            transaction.setTransactionType(TransactionType.convert(transactionNode.getAttributes().getNamedItem("type").getTextContent()));
            transactionList.add(transaction);
        }
        client.setServer(server);
        client.setTerminal(terminal);
        client.setTransactions(transactionList);
        Node node = document.getElementsByTagName("outLog").item(0).getAttributes().getNamedItem("path");

        client.setLogFileName(node.getTextContent());


        return client;
    }

    @Override
    public void run() {
        try {

            List<Response> responseList = null;
            Client client = readXml();
            FileHandler fileHandler = new FileHandler("term" + client.getTerminal().getTerminalId() + ".log");
            SimpleFormatter simpleFormatter = new SimpleFormatter();
            fileHandler.setFormatter(simpleFormatter);
            logger.addHandler(fileHandler);
            logger.setUseParentHandlers(false);
            List<Transaction> transactionList = client.getTransactions();
            // getting ip
            InetAddress serverIp = InetAddress.getByName(client.getServer().getServerIp());
            Socket socket = new Socket(serverIp, Integer.parseInt(client.getServer().getServerPortNumber()));
            logger.info("establish the connection with server port" + client.getServer().getServerPortNumber());
            // obtaining input and out streams
            DataInputStream dataInputStream = new DataInputStream(socket.getInputStream());
            DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
            ObjectInputStream objectInputStream = new ObjectInputStream(socket.getInputStream());
            objectOutputStream.writeObject(transactionList);
            logger.info("Transaction send to sever from " + socket);
            responseList = (List<Response>) objectInputStream.readObject();
            logger.info("Receive Response from server for " + socket);
            writeXml(responseList);
            objectInputStream.close();
            objectOutputStream.close();
            dataInputStream.close();
            dataOutputStream.close();


        } catch (Exception e) {
            e.printStackTrace();
            logger.throwing("Threads Class", "run", e);
        }

    }
}


