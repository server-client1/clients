package com.dotin.common;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        Clients firstClient = new Clients("firstClientInformationFile.xml", "firstClientResults.xml");
        Clients secondClient = new Clients("secondClientInformationFile.xml", "secondClientResults.xml");
        Thread firstClientThread = new Thread(firstClient);
        Thread secondClientThread = new Thread(secondClient);
        firstClientThread.start();
        secondClientThread.start();

    }
}
